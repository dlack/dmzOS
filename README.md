# dmzOS
## About
**dmzOS** is an operating system written by a group of computer enthusiasts gathered around [Decentrala](https://decentrala.org/). You are free to explore, learn, and contribute! 

### Description
* Written from scratch in C and asm.
* POSIX-compliant (as we are aspiring to [Stan Dard](https://wiki.osdev.org/Stan_Dard)).
* For the amd64 architecture.
* Extensively documented.

### Goals
* Sharpen low-level programming skills.
* Familiarize with amd64 architecture details.
* Have fun along the way!

### Success criteria
* Run vim on the OS.

### Rules
* Please provide an adequate entry in the Wiki for each non-trivial code contribution.

To learn more about the project, please refer to the [Wiki](https://gitea.dmz.rs/sborovic/dmzOS/wiki).

## DevOps
### Development environment
Essential software (plus dependencies) that are needed for the development are available through a custom Ubuntu-based Docker image (located in `Dockerfiles/dockerfile.c_toolchain` as well as on Docker Hub). This includes:
- `gcc`: *The GNU Compiler Collection.*
- `indent` GNU C code formatter.
- `clang`: a compiler front end for the C, C++, Objective-C, and Objective-C++ programming languages
- `clang-format`: a tool to automatically format C/C++/Objective-C code
- `gdb`: *The GNU Debugger.*
- `make`: a tool which controls the generation of executables and other non-source files of a program from the program's source files.
- `objdump`: a command-line program for displaying various information about object files.
- `nasm`: *The Netwide Assembler* is an assembler and disassembler for the Intel x86 architecture. 
- `xxd`: a command-line program that may be used to manage, convert, and display binary files.
- `ld`: *The GNU Linker*.
- `lld`: a linker from the LLVM project that is a drop-in replacement for system linkers and runs much faster than them.
- `vim`: a highly configurable text editor.
> *Note*
> A `.vimrc` file with a rule to format code on buffer save in line with *Kernighan and Ritchie style* (with 4-space indentation) comes pre-installed inside the image.

Booting the latest `.iso` image of *dmzOS* is done using a [Dockerized QEMU](https://github.com/qemus/qemu-docker).

#### Command cheatsheet
* To get into a `bash` shell that is bound to the `dmzOS` directory:
```docker
docker-compose run --rm c_toolchain
```
> *Note*
> Any changes in the container's `dmzOS` directory will be reflected in the local `dmzOS` directory.
